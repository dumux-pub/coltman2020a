reset
set term pngcairo size 1600,1200 solid font "arial,36"
set output "./FlatTests_RE_Evap.png"
set datafile separator ';'
DATA='./'
set ylabel "Evaporation Rate [mm/d]"
set xlabel "time [days]"
set yrange [0:30]
set xrange [0:8]
set key title "Reynolds Number:"
set key right top samplen 1

plot    DATA.'storage_FlatSurface_Re1e4.csv'    u ($1/86400):4  w l lw 5 lc rgb "light-red"    t '1e4',\
        DATA.'storage_FlatSurface_Re1e5.csv'    u ($1/86400):4  w l lw 5 lc rgb "turquoise"    t '1e5',\
        DATA.'storage_FlatSurface_Re2e5.csv'    u ($1/86400):4  w l lw 5 lc rgb "blue"         t '2e5',\
        DATA.'storage_FlatSurface_Re5e5.csv'    u ($1/86400):4  w l lw 5 lc rgb "dark-green"   t '5e5',\
        DATA.'storage_FlatSurface_Re1e6.csv'    u ($1/86400):4  w l lw 5 lc rgb "chartreuse"   t '1e6',\
