reset
set term pngcairo size 1600,1200 solid font "arial,36"
set output "./FlatTests_RANS_Evap.png"
set datafile separator ';'
DATA='./'
set ylabel "Evaporation Rate [mm/d]"
set xlabel "time [days]"
set yrange [2:5]
set xrange [0:6]
set key title "Turbulence Model:"
set key right top reverse samplen 1

plot    DATA.'storage_FlatSurface_KEpsilon.csv'         u ($1/86400):4  w l lw 5 lc rgb "dark-red" t    'K-Epsilon (Two-Eq)',\
        DATA.'storage_FlatSurface_KOmega.csv'           u ($1/86400):4  w l lw 5 lc rgb "orange" t      'K-Omega (Two-Eq)',\
        DATA.'storage_FlatSurface_LowReKEpsilon.csv'    u ($1/86400):4  w l lw 5 lc rgb "green"  t      'Low RE K-Epsilon (Two-Eq)',\
        DATA.'storage_FlatSurface_OneEq.csv'            u ($1/86400):4  w l lw 5 lc rgb "cyan"   t      'Spallart-Amaras (One-Eq)',\
        DATA.'storage_FlatSurface_ZeroEq.csv'           u ($1/86400):4  w l lw 5 lc rgb "purple" t      'Baldwin-Lomax (Zero-Eq)'
