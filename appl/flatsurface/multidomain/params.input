[TimeLoop]
DtInitial =  1e-6 # [s]
MaxTimeStepSize = 7200 # [s] (2 hour)
TEnd = 518400 # [s] (6 days)

[Grid]
Positions0 = -2  0.0   1.0   3.0
Cells0 =          10    30    10
Grading0 =     -1.25  1.00  1.25
Positions1 = 0   0.5   1.0
Cells1 =          25    25
Grading1 =     -1.30  1.30

StartDarcy = 0.0 0.5
EndDarcy = 1.0 0.5

[RANS.Problem]
Name = rans
InletVelocity = 5. # [m/s]
InletPressure = 1e5 # [Pa]
InletMassFrac = 0.008 # [-]
Temperature = 298.15 # [K]

[Darcy.Problem]
Name = darcy
Pressure = 1e5 # [Pa]
Temperature = 298.15 # [K]
InitPhasePresence = 3 # bothPhases
InitialSaturation = 0.95 # [-]

[Darcy.SpatialParams]
Porosity = 0.41
Permeability = 2.65e-10
AlphaBeaversJoseph = 1.0
Swr = 0.005
Snr = 0.01
VgAlpha = 6.37e-4
VgN = 8.0

[Problem]
EnableGravity = true
InterfaceDiffusionCoefficientAvg = FreeFlowOnly # Harmonic

[RANS.RANS]
UseStoredEddyViscosity = false
EddyViscosityModel = "baldwinLomax"

[Vtk]
AddVelocity = true
WriteFaceData = true
OutputName = flat_interface

[Component]
SolidDensity = 2700
SolidThermalConductivity = 2.8
SolidHeatCapacity = 790

[Flux]
TvdApproach = Hou # 1: Uniform TVD, 2: Li's approach, 3: Hou's approach
DifferencingScheme = Vanleer # Vanleer , Vanalbada, Minmod, Superbee, Umist, Mclimiter, Wahyd

[MatrixConverter]
DeletePatternEntriesBelowAbsThreshold = 1e-25

[Newton]
TargetSteps = 10
MaxSteps = 10
MaxRelativeShift = 1e-6

[Assembly]
NumericDifferenceMethod = 0
NumericDifference.BaseEpsilon = 1e-8
