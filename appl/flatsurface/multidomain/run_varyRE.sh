runSim () {
./$1 $2 -RANS.Problem.InletVelocity $3 -Vtk.OutputName $4 | tee -a logfile_$4.out
}

runSim test_md_boundary_darcy2p2cni_rans1p2cnikomega  params_varyRe.input  0.30   FlatSurface_Re1e4
runSim test_md_boundary_darcy2p2cni_rans1p2cnikomega  params_varyRe.input  3.00   FlatSurface_Re1e5
runSim test_md_boundary_darcy2p2cni_rans1p2cnikomega  params_varyRe.input  6.00   FlatSurface_Re2e5
runSim test_md_boundary_darcy2p2cni_rans1p2cnikomega  params_varyRe.input  15.0   FlatSurface_Re5e5
runSim test_md_boundary_darcy2p2cni_rans1p2cnikomega  params_varyRe.input  30.0   FlatSurface_Re1e6
