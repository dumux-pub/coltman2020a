runSim () {
./$1 $2 -RANS.Problem.InletVelocity $3 -Vtk.OutputName $4 | tee -a logfile_$4.out
}

runSim test_md_boundary_darcy2p2cni_rans1p2cnizeroeq         params.input           1.0  FlatSurface_ZeroEq
runSim test_md_boundary_darcy2p2cni_rans1p2cnioneeq          params.input           1.0  FlatSurface_OneEq
runSim test_md_boundary_darcy2p2cni_rans1p2cnilowrekepsilon  params.input           1.0  FlatSurface_LowReKEpsilon
runSim test_md_boundary_darcy2p2cni_rans1p2cnikomega         params.input           1.0  FlatSurface_KOmega
runSim test_md_boundary_darcy2p2cni_rans1p2cnikepsilon       params_kepsilon.input  1.0  FlatSurface_KEpsilon
