runSim () {
 ./$1 $2 -Vtk.OutputName $3 | tee -a logfile_$3.out
restart=`ls -ltr $3_*vtu | tail -n 1 | awk '{print $9}'`
restart_face=`ls -ltr $3_*vtp | tail -n 1 | awk '{print $9}'`
cp $restart "rans_$3-restart.vtu"
cp $restart_face "rans_$3-face-restart.vtp"
}

runSim test_ff_rans2cni_obstacles_spacing params_spacing025.input obstacle_spacing_025
runSim test_ff_rans2cni_obstacles_spacing params_spacing050.input obstacle_spacing_050
runSim test_ff_rans2cni_obstacles_spacing params_spacing075.input obstacle_spacing_075
runSim test_ff_rans2cni_obstacles_spacing params_spacing100.input obstacle_spacing_100
runSim test_ff_rans2cni_obstacles_spacing params_spacing125.input obstacle_spacing_125
runSim test_ff_rans2cni_obstacles_spacing params_spacing150.input obstacle_spacing_150
