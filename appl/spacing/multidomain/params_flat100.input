[TimeLoop]
DtInitial =  1e-5 # [s]
MaxTimeStepSize = 7200 # [s]
TEnd = 360000 # [s]

[Grid]
Positions0 = -4  0.0   0.5  0.55  0.60   1.10   1.60  1.65  1.70   2.20   6.20
Cells0 =           8    25    10    10     32     32    10    10     25      8
Grading0 =     -1.30 -1.30   1.3  -1.3   1.21  -1.21   1.3  -1.3   1.30    1.3
Positions1 =  0  0.2    0.25  1.0
Cells1 =          15      20   15
Grading1 =     -1.30    1.15  1.3

StartDarcy = 0.0 0.2
StartFormOne = 0.50
EndFormOne = 0.60
StartFormTwo = 1.60
EndFormTwo = 1.70
EndDarcy = 2.20 0.2
Amplitude = 0.0

[RANS.Problem]
Name = rans
InletVelocity = 1. # [m/s]
InletPressure = 1e5 # [Pa]
InletMassFrac = 0.008 # [-]
Temperature = 298.15 # [K]

[Darcy.Problem]
Name = darcy
Pressure = 1e5
InitialSaturation = 0.95 # initial Sw
InitialSaturationAbove = 0.1
Temperature = 298.15 # [K]
InitPhasePresence = 3 # bothPhases

[Darcy.SpatialParams]
Porosity = 0.41
Permeability = 2.65e-10
AlphaBeaversJoseph = 1.0
Swr = 0.005
Snr = 0.01
VgAlpha = 6.37e-4
VgN = 8.0

[Problem]
EnableGravity = true
InterfaceDiffusionCoefficientAvg = FreeFlowOnly # Harmonic

[RANS.RANS]
UseStoredEddyViscosity = false

[Vtk]
AddVelocity = true
WriteFaceData = true
OutputName = multidomain_obstacle_flat_100

[Component]
SolidDensity = 2700
SolidThermalConductivity = 2.8
SolidHeatCapacity = 790

[Flux]
TvdApproach = Hou # 1: Uniform TVD, 2: Li's approach, 3: Hou's approach
DifferencingScheme = Vanleer # Vanleer , Vanalbada, Minmod, Superbee, Umist, Mclimiter, Wahyd

[MatrixConverter]
DeletePatternEntriesBelowAbsThreshold = 1e-25

[Newton]
MaxSteps = 10
MaxRelativeShift = 1e-6

[Assembly]
NumericDifferenceMethod = 0
NumericDifference.BaseEpsilon = 1e-8
