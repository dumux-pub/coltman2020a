// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the coupled RANS/Darcy problem
 */
#include <config.h>

#include <ctime>
#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>
#include <dune/grid/yaspgrid.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/partial.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/geometry/diameter.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/gridmanager_sub.hh>
#include <dumux/io/loadsolution.hh>

#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include <dumux/multidomain/boundary/stokesdarcy/couplingmanager.hh>
#include <dumux/freeflow/navierstokes/staggered/fluxoversurface.hh>

#include "problem_darcy.hh"
#include "problem_rans.hh"

namespace Dumux {
namespace Properties {

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::RANSOnePTwoC>
{
    using Traits = StaggeredMultiDomainTraits<TypeTag, TypeTag, Properties::TTag::DarcyTwoPTwoC>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::DarcyTwoPTwoC>
{
    using Traits = StaggeredMultiDomainTraits<Properties::TTag::RANSOnePTwoC, Properties::TTag::RANSOnePTwoC, TypeTag>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

} // end namespace Properties
} // end namespace Dumux

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using RANSTypeTag = Properties::TTag::RANSOnePTwoC;
    using DarcyTypeTag = Properties::TTag::DarcyTwoPTwoC;

    // The hostgrid
    constexpr int dim = 2;
    using GlobalPosition = Dune::FieldVector<double, dim>;
    using HostGrid = Dune::YaspGrid<2, Dune::TensorProductCoordinates<double, dim> >;
    using SubGrid = Dune::SubGrid<dim, HostGrid>;
    using HostGridManager = GridManager<HostGrid>;
    HostGridManager hostGridManager;
    hostGridManager.init();
    auto& hostGrid = hostGridManager.grid();

    struct Params
    {
        GlobalPosition startDarcy = getParam<GlobalPosition>("Grid.StartDarcy");
        GlobalPosition endDarcy = getParam<GlobalPosition>("Grid.EndDarcy");
        double startFormOne = getParam<double>("Grid.StartFormOne");
        double endFormOne = getParam<double>("Grid.EndFormOne");
        double startFormTwo = getParam<double>("Grid.StartFormTwo");
        double endFormTwo = getParam<double>("Grid.EndFormTwo");
        double amplitude = getParam<double>("Grid.Amplitude");
    };
    Params params;

    auto ransSelector = [&params](const auto& element)
    {
        double eps = 1e-12;
        GlobalPosition globalPos = element.geometry().center();
        const bool inFormOne = (globalPos[0] > params.startFormOne - eps && globalPos[0] < params.endFormOne + eps);
        const bool inFormTwo = (globalPos[0] > params.startFormTwo - eps && globalPos[0] < params.endFormTwo + eps);
        const bool inForm = (inFormOne || inFormTwo);
        if (globalPos[1] > ((params.startDarcy[1] + params.amplitude) - eps))
            return true;
        else if (globalPos [1] > (params.startDarcy[1] - eps))
        {
            if (inForm)
                return false;
            else
                return true;
        }
        else
            return false;
    };

    auto darcySelector = [&params](const auto& element)
    {
        double eps = 1e-12;
        GlobalPosition globalPos = element.geometry().center();
        const bool inEntranceExit = ((globalPos [0] < params.startDarcy[0] + eps) || (globalPos [0] > params.endDarcy[0] - eps));
        const bool inFormOne = (globalPos[0] > params.startFormOne - eps && globalPos[0] < params.endFormOne + eps);
        const bool inFormTwo = (globalPos[0] > params.startFormTwo - eps && globalPos[0] < params.endFormTwo + eps);
        const bool inForm = (inFormOne || inFormTwo);

        if (inEntranceExit)
            return false;
        else
        {
            if (globalPos[1] > ((params.startDarcy[1] + params.amplitude) - eps))
                return false;
            else if (globalPos [1] > (params.startDarcy[1] - eps))
            {
                if (!inForm)
                    return false;
                else
                    return true;
            }
            else
                return true;
        }
    };
    Dumux::GridManager<SubGrid> ransSubGridManager;
    Dumux::GridManager<SubGrid> darcySubGridManager;

    ransSubGridManager.init(hostGrid, ransSelector, "RANS");
    darcySubGridManager.init(hostGrid, darcySelector, "Darcy");

    // we compute on the leaf grid view
    const auto& ransGridView = ransSubGridManager.grid().leafGridView();
    const auto& darcyGridView = darcySubGridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using RANSGridGeometry = GetPropType<RANSTypeTag, Properties::GridGeometry>;
    auto ransGridGeometry = std::make_shared<RANSGridGeometry>(ransGridView);
    ransGridGeometry->update();
    using DarcyGridGeometry = GetPropType<DarcyTypeTag, Properties::GridGeometry>;
    auto darcyGridGeometry = std::make_shared<DarcyGridGeometry>(darcyGridView);
    darcyGridGeometry->update();

    using Traits = StaggeredMultiDomainTraits<RANSTypeTag, RANSTypeTag, DarcyTypeTag>;

    // the coupling manager
    using CouplingManager = StokesDarcyCouplingManager<Traits>;
    auto couplingManager = std::make_shared<CouplingManager>(ransGridGeometry, darcyGridGeometry);

    // the indices
    constexpr auto ransCellCenterIdx = CouplingManager::stokesCellCenterIdx;
    constexpr auto ransFaceIdx = CouplingManager::stokesFaceIdx;
    constexpr auto darcyIdx = CouplingManager::darcyIdx;

    // the problem (initial and boundary conditions)
    using RansProblem = GetPropType<RANSTypeTag, Properties::Problem>;
    auto ransProblem = std::make_shared<RansProblem>(ransGridGeometry, couplingManager);
    using DarcyProblem = GetPropType<DarcyTypeTag, Properties::Problem>;
    auto darcyProblem = std::make_shared<DarcyProblem>(darcyGridGeometry, couplingManager);

    // initialize the fluidsystem (tabulation)
    GetPropType<RANSTypeTag, Properties::FluidSystem>::init();

    // get some time loop parameters
    using Scalar = GetPropType<RANSTypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // instantiate time loop
    auto timeLoop = std::make_shared<TimeLoop<Scalar>>(0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // set timeloop for the subproblems, needed for boundary value variations
    ransProblem->setTimeLoop(timeLoop);
    darcyProblem->setTimeLoop(timeLoop);

    // the solution vector
    Traits::SolutionVector sol;
    sol[ransCellCenterIdx].resize(ransGridGeometry->numCellCenterDofs());
    sol[ransFaceIdx].resize(ransGridGeometry->numFaceDofs());
    sol[darcyIdx].resize(darcyGridGeometry->numDofs());
    auto ransSol = partial(sol, ransCellCenterIdx, ransFaceIdx);
    auto& darcySol = sol[darcyIdx];

    if (hasParam("Restart.DarcyFile"))
    {
        using IOFields = GetPropType<DarcyTypeTag, Properties::IOFields>;
        using ModelTraits = GetPropType<DarcyTypeTag, Properties::ModelTraits>;
        using PrimaryVariables = GetPropType<DarcyTypeTag, Properties::PrimaryVariables>;
        using FluidSystem = GetPropType<DarcyTypeTag, Properties::FluidSystem>;
        const auto restartFileName = getParam<std::string>("Restart.DarcyFile");
        loadSolution(darcySol, restartFileName, createCellCenterPVNameFunction<IOFields, PrimaryVariables, ModelTraits, FluidSystem>(), *darcyGridGeometry);
        std::cout << "finished darcy read in \n";
    }
    else
        darcyProblem->applyInitialSolution(darcySol);

    if (hasParam("Restart.RansCCFile") && hasParam("Restart.RansFaceFile"))
    {
        using IOFields = GetPropType<RANSTypeTag, Properties::IOFields>;
        using ModelTraits = GetPropType<RANSTypeTag, Properties::ModelTraits>;
        using CellCenterPrimaryVariables = GetPropType<RANSTypeTag, Properties::CellCenterPrimaryVariables>;
        using FacePrimaryVariables = GetPropType<RANSTypeTag, Properties::FacePrimaryVariables>;
        using FluidSystem = GetPropType<RANSTypeTag, Properties::FluidSystem>;

        // read in the CC rans solution
        const auto restartFileNameCC = getParam<std::string>("Restart.RansCCFile");
        loadSolution(ransSol[ransCellCenterIdx], restartFileNameCC, createCellCenterPVNameFunction<IOFields, CellCenterPrimaryVariables, ModelTraits, FluidSystem>(), *ransGridGeometry);
        std::cout << "finished rans cellcentered read in \n";

        // read in the face rans solution
        const auto restartFileNameFace = getParam<std::string>("Restart.RansFaceFile");
        loadSolution(ransSol[ransFaceIdx], restartFileNameFace, createFacePVNameFunction<IOFields, FacePrimaryVariables, ModelTraits, FluidSystem>(), *ransGridGeometry);
        std::cout << "finished rans Face read in \n";
    }
    else
        ransProblem->applyInitialSolution(ransSol);

    ransProblem->updateStaticWallProperties();
    ransProblem->updateDynamicWallProperties(ransSol);

    sol[darcyIdx] = darcySol;
    sol[ransCellCenterIdx] = ransSol[ransCellCenterIdx];
    sol[ransFaceIdx] = ransSol[ransFaceIdx];
    auto solOld = sol;

    // intialize the coupling manager
    couplingManager->init(ransProblem, darcyProblem, sol);

    // intializethe grid variables and the subproblems
    using RANSGridVariables = GetPropType<RANSTypeTag, Properties::GridVariables>;
    auto ransGridVariables = std::make_shared<RANSGridVariables>(ransProblem, ransGridGeometry);
    ransGridVariables->init(ransSol);
    using DarcyGridVariables = GetPropType<DarcyTypeTag, Properties::GridVariables>;
    auto darcyGridVariables = std::make_shared<DarcyGridVariables>(darcyProblem, darcyGridGeometry);
    darcyGridVariables->init(sol[darcyIdx]);

    // intialize the vtk output module
    StaggeredVtkOutputModule<RANSGridVariables, decltype(ransSol)> ransVtkWriter(*ransGridVariables, ransSol, ransProblem->name());
    GetPropType<RANSTypeTag, Properties::IOFields>::initOutputModule(ransVtkWriter);
    ransVtkWriter.write(0.0);

    VtkOutputModule<DarcyGridVariables, GetPropType<DarcyTypeTag, Properties::SolutionVector>> darcyVtkWriter(*darcyGridVariables, sol[darcyIdx], darcyProblem->name());
    using DarcyVelocityOutput = GetPropType<DarcyTypeTag, Properties::VelocityOutput>;
    darcyVtkWriter.addVelocityOutput(std::make_shared<DarcyVelocityOutput>(*darcyGridVariables));
    GetPropType<DarcyTypeTag, Properties::IOFields>::initOutputModule(darcyVtkWriter);
    darcyVtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(ransProblem, ransProblem, darcyProblem),
                                                 std::make_tuple(ransGridGeometry->cellCenterFVGridGeometryPtr(),
                                                                 ransGridGeometry->faceFVGridGeometryPtr(),
                                                                 darcyGridGeometry),
                                                 std::make_tuple(ransGridVariables->cellCenterGridVariablesPtr(),
                                                                 ransGridVariables->faceGridVariablesPtr(),
                                                                 darcyGridVariables),
                                                 couplingManager,
                                                 timeLoop);

    // initialize the output flux files
    darcyProblem->initMethod1(sol[darcyIdx], *darcyGridVariables);
    ransProblem->initMethod2();

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

    // time loop
    timeLoop->start(); do
    {
        // set previous solution for storage evaluations
        assembler->setPreviousSolution(solOld);

        // solve the non-linear system with time step control
        nonLinearSolver.solve(sol, *timeLoop);

        // make the new solution the old solution
        solOld = sol;

        // Method 1: Water Balance
            darcyProblem->fluxMethod1(sol[darcyIdx], *darcyGridVariables);
        // Method 2: Coupling Manager
            ransProblem->fluxMethod2(sol, *ransGridVariables, timeLoop->timeStepSize());

        // advance grid variables to the next time step
        ransGridVariables->advanceTimeStep();
        darcyGridVariables->advanceTimeStep();

        ransSol[ransCellCenterIdx] = sol[ransCellCenterIdx];
        ransSol[ransFaceIdx] = sol[ransFaceIdx];

        // update the auxiliary free flow solution vector
        ransProblem->updateDynamicWallProperties(ransSol);
        assembler->updateGridVariables(sol);

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // write vtk output
        ransVtkWriter.write(timeLoop->time());
        darcyVtkWriter.write(timeLoop->time());

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

    } while (!timeLoop->finished());

    timeLoop->finalize(ransGridView.comm());
    timeLoop->finalize(darcyGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
