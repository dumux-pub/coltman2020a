runSim () {
./$1 $2 -Vtk.OutputName $3 | tee -a logfile_$3.out
restart=`ls -ltr $3_*vtu | tail -n 1 | awk '{print $9}'`
restart_face=`ls -ltr $3_*vtp | tail -n 1 | awk '{print $9}'`
cp $restart "rans_$3-restart.vtu"
cp $restart_face "rans_$3-face-restart.vtp"
}

runSim test_ff_rans2cni_obstacles_permeability params.input obstacle_permeability_all
