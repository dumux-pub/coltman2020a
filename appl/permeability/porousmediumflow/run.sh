runSim () {
./$1 $2 -Vtk.OutputName $3 | tee -a logfile_$3.out
restart=`ls -ltr $3_*vtu | tail -n 1 | awk '{print $9}'`
cp $restart "darcy_$3-restart.vtu"
}

runSim test_pm_darcy2cni_permeability params_k07.input obstacle_permeability_07
runSim test_pm_darcy2cni_permeability params_k09.input obstacle_permeability_08
runSim test_pm_darcy2cni_permeability params_k09.input obstacle_permeability_09
runSim test_pm_darcy2cni_permeability params_k10.input obstacle_permeability_10
