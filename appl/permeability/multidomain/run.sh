runSim () {
./$1 $2 -Vtk.OutputName $3 | tee -a logfile_$3.out
}

runSim test_md_darcy2cni_rans2cni_permeability params_k10.input multidomain_obstacle_permeability_10
runSim test_md_darcy2cni_rans2cni_permeability params_k09.input multidomain_obstacle_permeability_09
runSim test_md_darcy2cni_rans2cni_permeability params_k08.input multidomain_obstacle_permeability_08
runSim test_md_darcy2cni_rans2cni_permeability params_k07.input multidomain_obstacle_permeability_07
