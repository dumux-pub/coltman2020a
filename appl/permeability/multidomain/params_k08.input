[Restart]
DarcyFile =     ../initialconditions/darcy_obstacle_permeability_08-restart.vtu
RansCCFile =    ../initialconditions/rans_obstacle_permeability_all-restart.vtu
RansFaceFile =  ../initialconditions/rans_obstacle_permeability_all-face-restart.vtp

[TimeLoop]
DtInitial =  5e-5 # [s]
MaxTimeStepSize = 7200 # [s]
TEnd = 360000 # [s]

[Grid]
Positions0 = -4   0.0    0.999     1.0  1.001   1.05    1.099   1.1 1.101   2.1    6.1
Cells0 =            8       30       3      3     14       14     3     3    30      8
Grading0 =      -1.30    -1.25    1.00   1.00    1.30   -1.30  1.00  1.00  1.25   1.30

Positions1 =  0  0.199    0.2  0.201  0.250   0.299  0.300  0.301  1.000
Cells1 =            15      3      3     20      20      3      3     25
Grading1 =       -1.30   1.00   1.00   1.20   -1.20   1.00   1.00   1.30

StartDarcy = 0.0 0.2
StartFormOne = 1.0
EndFormOne = 1.1
EndDarcy = 2.1 0.2
Amplitude = 0.100

[RANS.Problem]
Name = rans
InletVelocity = 1. # [m/s]
InletPressure = 1e5 # [Pa]
InletMassFrac = 0.008 # [-]
Temperature = 298.15 # [K]

[Darcy.Problem]
Name = darcy
Pressure = 1e5
InitialSaturation = 0.95 # initial Sw
MassFracAbove = 0.008
Temperature = 298.15 # [K]
InitPhasePresence = 3 # bothPhases
InitPhasePresenceAbove = 2 # gas phase

[Darcy.SpatialParams]
Permeability = 2.65e-10
Porosity = 0.41
AlphaBeaversJoseph = 1.0
Swr = 0.005
Snr = 0.005
VgAlpha = 6.37e-4
VgN = 8.0

SwrAbove = 0.005
SnrAbove = 0.005
PorosityAbove = 0.75
PermeabilityAbove = 1e-08
VgAlphaAbove = 0.1
VgNAbove = 100

[Problem]
EnableGravity = true
InterfaceDiffusionCoefficientAvg = FreeFlowOnly # Harmonic

[RANS.RANS]
UseStoredEddyViscosity = false

[Vtk]
AddVelocity = true
WriteFaceData = true
OutputName = multidomain_obstacle_permeability_08

[Component]
SolidDensity = 2700
SolidThermalConductivity = 2.8
SolidHeatCapacity = 790

[Flux]
TvdApproach = Hou # 1: Uniform TVD, 2: Li's approach, 3: Hou's approach
DifferencingScheme = Vanleer # Vanleer , Vanalbada, Minmod, Superbee, Umist, Mclimiter, Wahyd

[Newton]
MaxSteps = 10
MaxRelativeShift = 1e-6

[Assembly]
NumericDifferenceMethod = 0
NumericDifference.BaseEpsilon = 1e-8
