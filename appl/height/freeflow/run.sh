
runSim () {
./$1 $2 -Vtk.OutputName $3 | tee -a logfile_$3.out
restart=`ls -ltr $3_*vtu | tail -n 1 | awk '{print $9}'`
restart_face=`ls -ltr $3_*vtp | tail -n 1 | awk '{print $9}'`
cp $restart "rans_$3-restart.vtu"
cp $restart_face "rans_$3-face-restart.vtp"
}

runSim test_ff_rans2cni_obstacles_height params_height020.input obstacle_height_020
runSim test_ff_rans2cni_obstacles_height params_height040.input obstacle_height_040
# runSim test_ff_rans2cni_obstacles_height params_height060.input obstacle_height_060
# runSim test_ff_rans2cni_obstacles_height params_height080.input obstacle_height_080
# runSim test_ff_rans2cni_obstacles_height params_height100.input obstacle_height_100
