runSim () {
./$1 $2 -Vtk.OutputName $3 | tee -a logfile_$3.out
restart=`ls -ltr $3_*vtu | tail -n 1 | awk '{print $9}'`
restart_face=`ls -ltr $3_*vtp | tail -n 1 | awk '{print $9}'`
cp $restart "rans_$3-restart.vtu"
cp $restart_face "rans_$3-face-restart.vtp"
}

cd sawtooth
runSim              test_ff_rans2cni_twoshapes_sawtooth             params.input twoshapes_sawtooth
cd ..
cd sawtoothreverse
runSim              test_ff_rans2cni_twoshapes_sawtoothreverse      params.input twoshapes_sawtoothreverse
cd ..
cd triangle
runSim              test_ff_rans2cni_twoshapes_triangle             params.input twoshapes_triangle
cd ..
cd rectangle
runSim              test_ff_rans2cni_twoshapes_rectangle            params.input twoshapes_rectangle
cd ..
cd sinus
runSim              test_ff_rans2cni_twoshapes_sinus                params.input twoshapes_sinus
cd ..
