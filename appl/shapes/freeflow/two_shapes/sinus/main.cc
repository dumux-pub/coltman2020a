// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the coupled RANS/Darcy problem
 */
#include <config.h>

#include <ctime>
#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/partial.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/geometry/diameter.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/staggeredfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/gridmanager_sub.hh>

#include "problem.hh"

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using RANSTypeTag = Properties::TTag::RANSOnePTwoC;

    // The hostgrid
    constexpr int dim = 2;
    using GlobalPosition = Dune::FieldVector<double, dim>;
    using HostGrid = Dune::YaspGrid<2, Dune::TensorProductCoordinates<double, dim> >;
    using SubGrid = Dune::SubGrid<dim, HostGrid>;
    using HostGridManager = GridManager<HostGrid>;
    HostGridManager hostGridManager;
    hostGridManager.init();
    auto& hostGrid = hostGridManager.grid();

    struct Params
    {
        double amplitude = getParam<double>("Grid.Amplitude");
        GlobalPosition startDarcy = getParam<GlobalPosition>("Grid.StartDarcy");
        double startFormOne = getParam<double>("Grid.StartFormOne");
        double endFormOne = getParam<double>("Grid.EndFormOne");
        double startFormTwo = getParam<double>("Grid.StartFormTwo");
        double endFormTwo = getParam<double>("Grid.EndFormTwo");
        GlobalPosition endDarcy = getParam<GlobalPosition>("Grid.EndDarcy");
    };
    Params params;

    // The subgrid
    auto ransSelector = [&params](const auto& element)
    {
        double eps = 1e-12;
        GlobalPosition globalPos = element.geometry().center();

        const bool inFormOne = (globalPos[0] > params.startFormOne - eps && globalPos[0] < params.endFormOne + eps);
        const bool inFormTwo = (globalPos[0] > params.startFormTwo - eps && globalPos[0] < params.endFormTwo + eps);

        const double frequency = (params.endFormOne - params.startFormOne)/2.;
        assert(frequency == (params.endFormTwo - (params.endFormOne - params.startFormOne)/2.)/2. && "forms must have the same width");

        const double horizontalOffsetOne = (params.endFormOne - params.startFormOne)/2.;
        const double argOne = std::cos((globalPos[0] - horizontalOffsetOne) * M_PI / frequency);
        const bool isBelowCurveOne = globalPos[1] < ((params.amplitude/2. * argOne) + params.startDarcy[1] + params.amplitude/2. + eps);

        const double horizontalOffsetTwo = (params.endFormTwo - params.startFormTwo)/2.;
        const double argTwo  = std::cos((globalPos[0] - horizontalOffsetTwo) * M_PI / frequency);
        const bool isBelowCurveTwo = globalPos[1] < ((params.amplitude/2. * argTwo) + params.startDarcy[1] + params.amplitude/2. + eps);

        if (globalPos[1] < params.startDarcy[1] + eps)
            return false;
        else if (globalPos[1] > (params.startDarcy[1] + (params.amplitude) - eps))
            return true;
        else if (inFormOne)
            return isBelowCurveOne ? false : true;
        else if (inFormTwo)
            return isBelowCurveTwo ? false : true;
        else
            return true;
    };

    Dumux::GridManager<SubGrid> ransSubGridManager;
    ransSubGridManager.init(hostGrid, ransSelector, "RANS");

    // we compute on the leaf grid view
    const auto& ransGridView = ransSubGridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using RANSGridGeometry = GetPropType<RANSTypeTag, Properties::GridGeometry>;
    auto ransGridGeometry = std::make_shared<RANSGridGeometry>(ransGridView, "RANS");
    ransGridGeometry->update();

    // the problem (initial and boundary conditions)
    using RansProblem = GetPropType<RANSTypeTag, Properties::Problem>;
    auto ransProblem = std::make_shared<RansProblem>(ransGridGeometry);

    // initialize the fluidsystem (tabulation)
    GetPropType<RANSTypeTag, Properties::FluidSystem>::init();

    // get some time loop parameters
    using Scalar = GetPropType<RANSTypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // instantiate time loop
    auto timeLoop = std::make_shared<TimeLoop<Scalar>>(0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);
    ransProblem->setTimeLoop(timeLoop);

    // the solution vector
    using SolutionVector = GetPropType<RANSTypeTag, Properties::SolutionVector>;
    SolutionVector sol;
    sol[RANSGridGeometry::cellCenterIdx()].resize(ransGridGeometry->numCellCenterDofs());
    sol[RANSGridGeometry::faceIdx()].resize(ransGridGeometry->numFaceDofs());
    ransProblem->applyInitialSolution(sol);
    ransProblem->updateStaticWallProperties();
    ransProblem->updateDynamicWallProperties(sol);
    auto solOld = sol;

    // intializethe grid variables and the subproblems
    using RANSGridVariables = GetPropType<RANSTypeTag, Properties::GridVariables>;
    auto ransGridVariables = std::make_shared<RANSGridVariables>(ransProblem, ransGridGeometry);
    ransGridVariables->init(sol);

    // intialize the vtk output module
    StaggeredVtkOutputModule<RANSGridVariables, SolutionVector> ransVtkWriter(*ransGridVariables, sol, ransProblem->name());
    GetPropType<RANSTypeTag, Properties::IOFields>::initOutputModule(ransVtkWriter);
    ransVtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    using Assembler = StaggeredFVAssembler<RANSTypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(ransProblem, ransGridGeometry, ransGridVariables, timeLoop, solOld);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // time loop
    timeLoop->start(); do
    {
        // set previous solution for storage evaluations
        assembler->setPreviousSolution(solOld);

        // solve the non-linear system with time step control
        nonLinearSolver.solve(sol, *timeLoop);

        // make the new solution the old solution
        solOld = sol;
        ransGridVariables->advanceTimeStep();

        // update the auxiliary free flow solution vector
        ransProblem->updateDynamicWallProperties(sol);
        assembler->updateGridVariables(sol);

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // write vtk output
        ransVtkWriter.write(timeLoop->time());

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

    } while (!timeLoop->finished());


    timeLoop->finalize(ransGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
