// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
 /*!
  * \file
  * \brief The free-flow sub problem
  */
#ifndef DUMUX_RANS1P_SUBPROBLEM_HH
#define DUMUX_RANS1P_SUBPROBLEM_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/material/fluidsystems/1padapter.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

#include <dumux/discretization/staggered/freeflow/properties.hh>

#include <dumux/freeflow/rans/problem.hh>
#include <dumux/freeflow/turbulencemodel.hh>
#include <dumux/freeflow/turbulenceproperties.hh>

#include <dumux/freeflow/compositional/komegancmodel.hh>
#include <dumux/freeflow/rans/twoeq/komega/problem.hh>

#include <dumux/multidomain/boundary/stokesdarcy/couplingdata.hh>

namespace Dumux {

template <class TypeTag>
class FreeFlowSubProblem;

namespace Properties {

// Create new type tags
namespace TTag {
struct RANSOnePTwoC { using InheritsFrom = std::tuple<KOmegaNCNI, StaggeredFreeFlowModel>; };
} // end namespace TTag

// The fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::RANSOnePTwoC>
{
  using H2OAir = FluidSystems::H2OAir<GetPropType<TypeTag, Properties::Scalar>>;
  static constexpr auto phaseIdx = H2OAir::gasPhaseIdx; // simulate the water phase
  using type = FluidSystems::OnePAdapter<H2OAir, phaseIdx>;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::RANSOnePTwoC>
{
    static constexpr auto dim = 2;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using TensorGrid = Dune::YaspGrid<2, Dune::TensorProductCoordinates<Scalar, dim> >;
    using HostGrid = TensorGrid;
    using type = Dune::SubGrid<dim, HostGrid>;
};


template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::RANSOnePTwoC> { static constexpr int value = 10; };

// Use formulation based on mole fractions
template<class TypeTag>
struct UseMoles<TypeTag, TTag::RANSOnePTwoC> { static constexpr bool value = true; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::RANSOnePTwoC> { using type = Dumux::FreeFlowSubProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::RANSOnePTwoC> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::RANSOnePTwoC> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::RANSOnePTwoC> { static constexpr bool value = true; };

template<class TypeTag>
struct UpwindSchemeOrder<TypeTag, TTag::RANSOnePTwoC> { static constexpr int value = 2; };


} // end namespace Properties

/*!
 * \brief The free-flow sub problem
 */
template <class TypeTag>
class FreeFlowSubProblem : public RANSProblem<TypeTag>
{
    using ParentType = RANSProblem<TypeTag>;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename ModelTraits::Indices;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;

    struct Params
    {
        double amplitude = getParam<double>("Grid.Amplitude");
        GlobalPosition startDarcy = getParam<GlobalPosition>("Grid.StartDarcy");
        double startFormOne = getParam<double>("Grid.StartFormOne");
        double endFormOne = getParam<double>("Grid.EndFormOne");
        double startFormTwo = getParam<double>("Grid.StartFormTwo");
        double endFormTwo = getParam<double>("Grid.EndFormTwo");
        GlobalPosition endDarcy = getParam<GlobalPosition>("Grid.EndDarcy");
    };

    static constexpr auto transportEqIdx = Indices::conti0EqIdx + 1;
    static constexpr auto transportCompIdx = Indices::conti0EqIdx + 1;
    static constexpr auto dimWorld = GridGeometry::GridView::dimensionworld;

public:
    FreeFlowSubProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry, "RANS"), eps_(1e-6)
    {
        pressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletPressure");
        inletMassFrac_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletMassFrac");
        temperature_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Temperature");
        inletVelocity_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletVelocity");
        curveBuffer_ = getParam<Scalar>("Grid.CurveBuffer");

        Dumux::TurbulenceProperties<Scalar, GridGeometry::GridView::dimensionworld, true> turbulenceProperties;
        FluidState fluidState;
        const auto phaseIdx = 0;
        fluidState.setPressure(phaseIdx, 1e5);
        fluidState.setTemperature(temperature());
        fluidState.setMassFraction(phaseIdx, phaseIdx, inletMassFrac());
        Scalar density = FluidSystem::density(fluidState, 0);
        Scalar kinematicViscosity = FluidSystem::viscosity(fluidState, 0) / density;
        Scalar charLength = this->gridGeometry().bBoxMax()[1] - this->gridGeometry().bBoxMin()[1];
        turbulentKineticEnergy_ = turbulenceProperties.turbulentKineticEnergy(inletVelocity_, charLength, kinematicViscosity, true);
        dissipation_ = turbulenceProperties.dissipationRate(inletVelocity_, charLength, kinematicViscosity, true);
        std::cout << std::endl;
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");

        std::cout << "The reynolds Number (l_char = channel diameter) is: " << inletVelocity_ * charLength / kinematicViscosity << "\n";

        const auto gridView = this->gridGeometry().gridView();
        auto fvGeometry = localView(this->gridGeometry());

        wallFacePositions_.clear();
        for (const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            for (const auto& scvf : scvfs(fvGeometry))
            {
                if (isOnWall(scvf))
                {
                    wallFacePositions_.push_back(scvf.ipGlobal());
                }
            }
        }
        std::cout << " there are " << wallFacePositions_.size() << " faces adjacent to a wall in this simulation \n";

        turbulenceModelName_ = turbulenceModelToString(ModelTraits::turbulenceModel());
        std::cout << "Using the "<< turbulenceModelName_ << " Turbulence Model. \n";
        std::cout << std::endl;

        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
    }

    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        return problemName_;
    }

    /*!
     * \brief The name of the turbulent model being used.
     */
    const std::string& ransName() const
    {
        return turbulenceModelName_;
    }

    /*!
     * \name Boundary Locations
     */
    // \{

    bool isOnWall(const SubControlVolumeFace &scvf) const
    {
        GlobalPosition globalPos = scvf.ipGlobal();
        if (scvf.boundary())
            return isOnWallAtPos(globalPos);
        else
            return false;
    }

    bool isOnWallAtPos(const GlobalPosition &globalPos) const
    {
        return (onLowerBoundary_(globalPos) || onUpperBoundary_(globalPos));
    }
    // \}


   /*!
     * \name Problem parameters
     */
    // \{

    //! \brief Return the temperature within the domain in [K].
    const Scalar temperature() const
    { return temperature_; }

    //! \brief Returns the inlet velocity.
    const Scalar inletVelocity() const
    { return inletVelocity_ ;}

    //! \brief Returns the inlet pressure.
    const Scalar inletPressure() const
    { return pressure_; }

    //! \brief Returns the inlet mass fraction.
    const Scalar inletMassFrac() const
    { return inletMassFrac_; }

   /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    { return NumEqVector(0.0); }

    // \}
   /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param element The finite element
     * \param scvf The sub control volume face
     */
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes bTypes;
        GlobalPosition globalPos = scvf.ipGlobal();

        // turbulence model-specific boundary types
        static constexpr auto numEq = numTurbulenceEq(ModelTraits::turbulenceModel());
        setBcTypes_(bTypes, globalPos, Dune::index_constant<numEq>{});

        if (isInlet_(globalPos))
        {
            bTypes.setDirichlet(Indices::velocityXIdx);
            bTypes.setDirichlet(Indices::velocityYIdx);
            bTypes.setDirichlet(Indices::temperatureIdx);
            bTypes.setDirichlet(transportCompIdx);
        }
        else if (isOutlet_(globalPos))
        {
            bTypes.setDirichlet(Indices::pressureIdx);
            bTypes.setOutflow(transportEqIdx);
            bTypes.setOutflow(Indices::energyEqIdx);
        }
        else if (onUpperBoundary_(globalPos))
			bTypes.setAllSymmetry();
        else
        {
            bTypes.setDirichlet(Indices::velocityXIdx);
            bTypes.setDirichlet(Indices::velocityYIdx);
            bTypes.setNeumann(transportEqIdx);
            bTypes.setDirichlet(Indices::temperatureIdx);
        }

        return bTypes;
    }

    /*!
     * \brief Returns whether a fixed Dirichlet value shall be used at a given cell.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param scv The sub control volume
     */
    template<class Element, class FVElementGeometry, class SubControlVolume>
    bool isDirichletCell(const Element& element,
                         const FVElementGeometry& fvGeometry,
                         const SubControlVolume& scv,
                         int pvIdx) const
    {
        using IsKOmegaKEpsilon = std::integral_constant<bool, (ModelTraits::turbulenceModel() == TurbulenceModel::komega
                                                            || ModelTraits::turbulenceModel() == TurbulenceModel::kepsilon)>;
        return isDirichletCell_(element, fvGeometry, scv, pvIdx, IsKOmegaKEpsilon{});
    }

    /*!
     * \brief Evaluate the boundary conditions for fixed values at cell centers
     *
     * \param element The finite element
     * \param scv the sub control volume
     * \note used for cell-centered discretization schemes
     */
    template<bool enable = (ModelTraits::turbulenceModel() == TurbulenceModel::komega
                         || ModelTraits::turbulenceModel() == TurbulenceModel::kepsilon),
                         std::enable_if_t<!enable, int> = 0>
    PrimaryVariables dirichlet(const Element& element, const SubControlVolume& scv) const
    {
        const auto globalPos = scv.center();
        PrimaryVariables values(initialAtPos(globalPos));

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for fixed values at cell centers
     *
     * \param element The finite element
     * \param scv the sub control volume
     * \note used for cell-centered discretization schemes
     */
    template<bool enable = (ModelTraits::turbulenceModel() == TurbulenceModel::komega
                         || ModelTraits::turbulenceModel() == TurbulenceModel::kepsilon),
                         std::enable_if_t<enable, int> = 0>
    PrimaryVariables dirichlet(const Element& element, const SubControlVolume& scv) const
    {
        using SetDirichletCellForBothTurbEq = std::integral_constant<bool, (ModelTraits::turbulenceModel() == TurbulenceModel::kepsilon)>;

        return dirichletTurbulentTwoEq_(element, scv, SetDirichletCellForBothTurbEq{});
    }

     /*!
      * \brief Evaluate the boundary conditions for a dirichlet values at the boundary.
      *
      * \param element The finite element
      * \param scvf the sub control volume face
      */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolumeFace& scvf) const
    {
        const auto globalPos = scvf.ipGlobal();
        PrimaryVariables values(initialAtPos(globalPos));

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param elemFaceVars The element face variables
     * \param scvf The boundary sub control volume face
     */
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace& scvf) const
    {
        PrimaryVariables values(0.0);
        return values;
    }


    // \}

   /*!
     * \name Volume terms
     */
    // \{

   /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);

        FluidState fluidState;
        fluidState.setPressure(0, pressure_);
        fluidState.setTemperature(temperature());
        fluidState.setMoleFraction(0, 0, 1);
        Scalar density = FluidSystem::density(fluidState, 0);
        values[Indices::pressureIdx] = pressure_ - density * this->gravity()[dimWorld-1]* (this->gridGeometry().bBoxMax()[1] - globalPos[1]);

        values[transportCompIdx] = inletMassFrac();
        values[Indices::temperatureIdx] = temperature();

        // block velocity profile
        values[Indices::velocityXIdx] = inletVelocity_;

        if (betweenForms_(globalPos) && aboveForms_(globalPos))
            values[Indices::velocityXIdx] = inletVelocity_ * height_() / (height_() - params_.amplitude);

        if (betweenForms_(globalPos) && !aboveForms_(globalPos))
            values[Indices::velocityXIdx] = 0.0;

        if (isOnWallAtPos(globalPos))
            values[Indices::velocityXIdx] = 0.0;

        // turbulence model-specific initial conditions
        static constexpr auto numEq = numTurbulenceEq(ModelTraits::turbulenceModel());
        setInitialAtPos_(values, globalPos, Dune::index_constant<numEq>{});

        return values;
    }

    void setTimeLoop(TimeLoopPtr timeLoop)
    { timeLoop_ = timeLoop; }

    // \}

private:
    bool isInlet_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }

    bool isOutlet_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
		if (onBase_(globalPos) && !inForm_(globalPos))
			return true;
		else if (inFormOne_(globalPos) && isBelowCurveOne_(globalPos))
			return true;
		else if (inFormTwo_(globalPos) && isBelowCurveTwo_(globalPos))
			return true;
		else
			return false;
    }

    bool onBase_(const GlobalPosition &globalPos) const
    { return globalPos[1] < (params_.startDarcy[1]  + eps_); }

    bool inForm_(const GlobalPosition &globalPos) const
    {
        if ((globalPos[1] > (params_.startDarcy[1] + params_.amplitude - eps_)) && (globalPos[1] > (params_.startDarcy[1] - eps_)))
        {
            if (((globalPos[0] > (params_.startFormOne - eps_)) && (globalPos[0] < (params_.endFormOne + eps_)))
             || ((globalPos[0] > (params_.startFormTwo - eps_)) && (globalPos[0] < (params_.endFormTwo + eps_))))
                return true;
        }

        return false;
    }

    bool inFormOne_(const GlobalPosition &globalPos) const
    { return ((globalPos[0] > (params_.startFormOne - eps_)) && (globalPos[0] < (params_.endFormOne + eps_))); }

    bool inFormTwo_(const GlobalPosition &globalPos) const
    { return ((globalPos[0] > (params_.startFormTwo - eps_)) && (globalPos[0] < (params_.endFormTwo + eps_))); }

    bool isBelowCurveOne_(const GlobalPosition &globalPos) const
    {
	    const Scalar frequency = params_.endFormOne - params_.startFormOne;
	    const Scalar argOne = std::cos((globalPos[0] - params_.startFormOne) * M_PI / frequency);
        return globalPos[1] < ((params_.amplitude * argOne) + params_.startDarcy[1] + curveBuffer_ + eps_);
    }

    bool isBelowCurveTwo_(const GlobalPosition &globalPos) const
    {
	    const Scalar frequency = params_.endFormTwo - params_.startFormTwo;
		const Scalar argTwo = std::cos((globalPos[0] - params_.startFormTwo) * M_PI / frequency);
        return globalPos[1] < ((params_.amplitude * argTwo) + params_.startDarcy[1] + curveBuffer_ + eps_);
    }

    bool betweenForms_(const GlobalPosition &globalPos) const
    { return ((globalPos[0] > (params_.startFormOne - eps_)) && (globalPos[0] < (params_.endFormTwo + eps_))); }

    bool aboveForms_(const GlobalPosition &globalPos) const
    { return globalPos[1] > (params_.startDarcy[1] + params_.amplitude - eps_); }

    // the height of the free-flow domain
    const Scalar height_() const
    { return this->gridGeometry().bBoxMax()[1] - this->gridGeometry().bBoxMin()[1]; }

    Scalar eps_;

    Params params_;

    Scalar inletReynoldsNumber_;
    Scalar inletVelocity_;
    Scalar pressure_;
    Scalar inletMassFrac_;
    Scalar temperature_;
    Scalar turbulentKineticEnergy_;
    Scalar dissipation_;
    Scalar viscosityTilde_;
    Scalar curveBuffer_;
    std::vector<GlobalPosition> wallFacePositions_;
    std::string problemName_;
    std::string turbulenceModelName_;

    TimeLoopPtr timeLoop_;

   /*!
     * \name Turbulent Boundary and Initial Conditions
     */
    // \{

    //! Initial conditions for the zero-eq turbulence model (none)
    void setInitialAtPos_(PrimaryVariables& values, const GlobalPosition &globalPos, Dune::index_constant<0>) const {}

    //! Initial conditions for the one-eq turbulence model
    void setInitialAtPos_(PrimaryVariables& values, const GlobalPosition &globalPos, Dune::index_constant<1>) const
    {
        values[Indices::viscosityTildeIdx] = viscosityTilde_;
        if (isOnWallAtPos(globalPos))
            values[Indices::viscosityTildeIdx] = 0.0;
    }

    //! Initial conditions for the komega, kepsilon and lowrekepsilon turbulence models
    void setInitialAtPos_(PrimaryVariables& values, const GlobalPosition &globalPos, Dune::index_constant<2>) const
    {
        values[Indices::turbulentKineticEnergyIdx] = turbulentKineticEnergy_;
        values[Indices::dissipationIdx] = dissipation_;
        if (isOnWallAtPos(globalPos))
        {
            values[Indices::turbulentKineticEnergyIdx] = 0.0;
        }
    }


    //! Boundary condition types for the komega, kepsilon and lowrekepsilon turbulence models
    void setBcTypes_(BoundaryTypes& values,const GlobalPosition& pos, Dune::index_constant<2>) const
    {
        if(isOutlet_(pos))
        {
            values.setOutflow(Indices::turbulentKineticEnergyEqIdx);
            values.setOutflow(Indices::dissipationEqIdx);
        }
        else
        {
            // walls and inflow
            values.setDirichlet(Indices::turbulentKineticEnergyIdx);
            values.setDirichlet(Indices::dissipationIdx);
        }
    }

    //! Forward to ParentType
    template<class Element, class FVElementGeometry, class SubControlVolume>
    bool isDirichletCell_(const Element& element,
                          const FVElementGeometry& fvGeometry,
                          const SubControlVolume& scv,
                          int pvIdx,
                          std::false_type) const
    {
        return ParentType::isDirichletCell(element, fvGeometry, scv, pvIdx);
    }

    //! Specialization for the KOmega and KEpsilon Models
    template<class Element, class FVElementGeometry, class SubControlVolume>
    bool isDirichletCell_(const Element& element,
                          const FVElementGeometry& fvGeometry,
                          const SubControlVolume& scv,
                          int pvIdx,
                          std::true_type) const
    {
        using SetDirichletCellForBothTurbEq = std::integral_constant<bool, (ModelTraits::turbulenceModel() == TurbulenceModel::kepsilon)>;
        return isDirichletCellTurbulentTwoEq_(element, fvGeometry, scv, pvIdx, SetDirichletCellForBothTurbEq{});
    }

    //! Specialization for the KEpsilon Model
    template<class Element>
    bool isDirichletCellTurbulentTwoEq_(const Element& element,
                                        const FVElementGeometry& fvGeometry,
                                        const SubControlVolume& scv,
                                        int pvIdx,
                                        std::true_type) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);

        // set a fixed turbulent kinetic energy and dissipation near the wall
        if (this->inNearWallRegion(eIdx))
            return pvIdx == Indices::turbulentKineticEnergyEqIdx || pvIdx == Indices::dissipationEqIdx;

        // set a fixed dissipation at  the matching point
        if (this->isMatchingPoint(eIdx))
            return pvIdx == Indices::dissipationEqIdx;// set a fixed dissipation (omega) for all cells at the wall

        return false;
    }

    //! Specialization for the KOmega Model
    template<class Element>
    bool isDirichletCellTurbulentTwoEq_(const Element& element,
                                        const FVElementGeometry& fvGeometry,
                                        const SubControlVolume& scv,
                                        int pvIdx,
                                        std::false_type) const
    {
        // set a fixed dissipation (omega) for all cells at the wall
        for (const auto& scvf : scvfs(fvGeometry))
            if (isOnWallAtPos(scvf.ipGlobal()) && pvIdx == Indices::dissipationIdx)
                return true;

        return false;

    }

    //! Specialization for the kepsilon
    template<class Element, class SubControlVolume>
    PrimaryVariables dirichletTurbulentTwoEq_(const Element& element,
                                              const SubControlVolume& scv,
                                              std::true_type) const
    {
        const auto globalPos = scv.center();
        PrimaryVariables values(initialAtPos(globalPos));
        unsigned int  elementIdx = this->gridGeometry().elementMapper().index(element);

        // fixed value for the turbulent kinetic energy
        values[Indices::turbulentKineticEnergyEqIdx] = this->turbulentKineticEnergyWallFunction(elementIdx);

        // fixed value for the dissipation
        values[Indices::dissipationEqIdx] = this->dissipationWallFunction(elementIdx);

        return values;
    }

    //! Specialization for the KOmega
    template<class Element, class SubControlVolume>
    PrimaryVariables dirichletTurbulentTwoEq_(const Element& element,
                                              const SubControlVolume& scv,
                                              std::false_type) const
    {
        const auto globalPos = scv.center();
        PrimaryVariables values(initialAtPos(globalPos));
        unsigned int  elementIdx = this->gridGeometry().elementMapper().index(element);

        const auto wallDistance = ParentType::wallDistance_[elementIdx];
        using std::pow;
        values[Indices::dissipationEqIdx] = 6.0 * ParentType::kinematicViscosity_[elementIdx]
                                                / (ParentType::betaOmega() * pow(wallDistance, 2));
        return values;
    }


    // \}

};
} //end namespace Dumux

#endif // DUMUX_RANS1P_SUBPROBLEM_HH
