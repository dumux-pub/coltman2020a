runSim () {
./$1 $2 -Vtk.OutputName $3 | tee -a logfile_$3.out
restart=`ls -ltr $3_*vtu | tail -n 1 | awk '{print $9}'`
restart_face=`ls -ltr $3_*vtp | tail -n 1 | awk '{print $9}'`
cp $restart "rans_$3-restart.vtu"
cp $restart_face "rans_$3-face-restart.vtp"
}

cd sawtooth
runSim             test_ff_rans2cni_shapes_sawtooth_single        params.input shapes_sawtooth_single
cd ..
cd sawtoothreverse
runSim             test_ff_rans2cni_shapes_sawtoothreverse_single params.input shapes_sawtoothreverse_single
cd ..
cd triangle
runSim             test_ff_rans2cni_shapes_triangle_single        params.input shapes_triangle_single
cd ..
cd rectangle
runSim             test_ff_rans2cni_shapes_rectangle_single       params.input shapes_rectangle_single
cd ..
cd sinus
runSim             test_ff_rans2cni_shapes_sinus_single           params.input shapes_sinus_single
cd ..

