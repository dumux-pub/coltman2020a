runSim () {
./$1 $2 -Vtk.OutputName $3 | tee -a logfile_$3.out
}

cd sawtoothreverse
runSim  test_md_darcy2cni_rans2cni_twoshapes_sawtoothreverse  params.input twoshapes_sawtoothreverse
cd ..
cd triangle
runSim  test_md_darcy2cni_rans2cni_twoshapes_triangle         params.input twoshapes_triangle
cd ..
cd rectangle
runSim  test_md_darcy2cni_rans2cni_twoshapes_rectangle        params.input twoshapes_rectangle
cd ..
cd sawtooth
runSim  test_md_darcy2cni_rans2cni_twoshapes_sawtooth         params.input twoshapes_sawtooth
cd ..
cd sinus
runSim  test_md_darcy2cni_rans2cni_twoshapes_sinus            params.input twoshapes_sinus
cd ..
