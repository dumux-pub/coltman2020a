[TimeLoop]
DtInitial =  1e-1 # [s]
MaxTimeStepSize = 60 # [s] (1 min)
TEnd = 120 # [s] (2 minutes)

[Grid]
Positions0 = -4  0.0    0.3  0.4  0.5   0.6   0.7    1.0  5.0
Cells0 =          12     15   25   10    10    25     15   12
Grading0 =      -1.3  -1.25  1.0  1.2  -1.2   1.0   1.25  1.3
Positions1 = 0  0.2  0.25   0.3   0.8
Cells1 =         20    25    25    15
Grading1 =    -1.25   1.1  -1.1   1.4

StartDarcy = 0.0 0.2
StartFormOne = 0.3
EndFormOne = 0.4
StartFormTwo = 0.60
EndFormTwo = 0.70
EndDarcy = 1.0 0.2
Amplitude = 0.1
CurveBuffer = 0.008

[Darcy.Problem]
Name = darcy
Pressure = 1e5
InitialSaturation = 0.95 # initial Sw
InitialSaturationAbove = 0.1
Temperature = 298.15 # [K]
InitPhasePresence = 3 # bothPhases

[Darcy.SpatialParams]
Porosity = 0.41
Permeability = 2.65e-10
AlphaBeaversJoseph = 1.0
Swr = 0.005
Snr = 0.01
VgAlpha = 6.37e-4
VgN = 8.0

[Problem]
EnableGravity = true

[Vtk]
AddVelocity = true
WriteFaceData = true
OutputName = twoshapes_triangle

[Component]
SolidDensity = 2700
SolidThermalConductivity = 2.8
SolidHeatCapacity = 790

[MatrixConverter]
DeletePatternEntriesBelowAbsThreshold = 1e-25

[Newton]
MaxSteps = 10
MaxRelativeShift = 1e-6

[Assembly]
NumericDifferenceMethod = 0
NumericDifference.BaseEpsilon = 1e-8
