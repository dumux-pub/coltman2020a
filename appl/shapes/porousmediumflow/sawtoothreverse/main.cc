// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the coupled RANS/Darcy problem
 */
#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

#include <dumux/discretization/method.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/gridmanager_sub.hh>

#include "problem.hh"

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using DarcyTypeTag = Properties::TTag::DarcyTwoPTwoC;

    // The hostgrid
    constexpr int dim = 2;
    using GlobalPosition = Dune::FieldVector<double, dim>;
    using HostGrid = Dune::YaspGrid<2, Dune::TensorProductCoordinates<double, dim> >;
    using SubGrid = Dune::SubGrid<dim, HostGrid>;
    using HostGridManager = GridManager<HostGrid>;
    HostGridManager hostGridManager;
    hostGridManager.init();
    auto& hostGrid = hostGridManager.grid();

    struct Params
    {
        double amplitude = getParam<double>("Grid.Amplitude");
        GlobalPosition startDarcy = getParam<GlobalPosition>("Grid.StartDarcy");
        double startFormOne = getParam<double>("Grid.StartFormOne");
        double endFormOne = getParam<double>("Grid.EndFormOne");
        double startFormTwo = getParam<double>("Grid.StartFormTwo");
        double endFormTwo = getParam<double>("Grid.EndFormTwo");
        GlobalPosition endDarcy = getParam<GlobalPosition>("Grid.EndDarcy");
    };
    Params params;

    // The subgrid
    auto darcySelector = [&params](const auto& element)
    {
        using std::floor;
        double eps = 1e-12;
        GlobalPosition globalPos = element.geometry().center();

        const bool inEntranceExit = ((globalPos [0] < params.startDarcy[0] + eps) || (globalPos [0] > params.endDarcy[0] - eps));

        const bool inFormOne = (globalPos[0] > params.startFormOne - eps && globalPos[0] < params.endFormOne + eps);
        const bool inFormTwo = (globalPos[0] > params.startFormTwo - eps && globalPos[0] < params.endFormTwo + eps);

        const double frequency = (params.endFormOne - params.startFormOne);
        assert(frequency == (params.endFormTwo - params.startFormTwo) && "forms must have the same width");

        const double argOne = (((globalPos[0]- params.startFormTwo)/frequency));
        const bool isBelowCurveOne = globalPos[1] < ((params.amplitude * (floor(argOne)- argOne) + params.amplitude + params.startDarcy[1]) + eps);

        const double argTwo = (((globalPos[0]- params.startFormTwo)/frequency));
        const bool isBelowCurveTwo = globalPos[1] < ((params.amplitude * (floor(argTwo)- argTwo) + params.amplitude + params.startDarcy[1]) + eps);

        if (inEntranceExit)
            return false;
        else
        {
            if (globalPos[1] > ((params.startDarcy[1] + params.amplitude) - eps))
                return false;
            else if (globalPos [1] > (params.startDarcy[1] - eps))
            {
                if (inFormOne)
                    return isBelowCurveOne;
                else if (inFormTwo)
                    return isBelowCurveTwo;
                else
                    return false;
            }
            else
                return true;
        }
    };

    Dumux::GridManager<SubGrid> darcySubGridManager;
    darcySubGridManager.init(hostGrid, darcySelector, "Darcy");

    // we compute on the leaf grid view
    const auto& darcyGridView = darcySubGridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<DarcyTypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(darcyGridView);
    gridGeometry->update();

    // the problem (initial and boundary conditions)
    using DarcyProblem = GetPropType<DarcyTypeTag, Properties::Problem>;
    auto darcyProblem = std::make_shared<DarcyProblem>(gridGeometry);

    // the solution vector    // the solution vector
    using SolutionVector = GetPropType<DarcyTypeTag, Properties::SolutionVector>;
    SolutionVector sol(gridGeometry->numDofs());
    darcyProblem->applyInitialSolution(sol);
    auto solOld = sol;

    // intializethe grid variables and the subproblems
    using DarcyGridVariables = GetPropType<DarcyTypeTag, Properties::GridVariables>;
    auto darcyGridVariables = std::make_shared<DarcyGridVariables>(darcyProblem, gridGeometry);
    darcyGridVariables->init(sol);

    // intialize the vtk output module
    VtkOutputModule<DarcyGridVariables, GetPropType<DarcyTypeTag, Properties::SolutionVector>> darcyVtkWriter(*darcyGridVariables, sol, darcyProblem->name());
    using DarcyVelocityOutput = GetPropType<DarcyTypeTag, Properties::VelocityOutput>;
    darcyVtkWriter.addVelocityOutput(std::make_shared<DarcyVelocityOutput>(*darcyGridVariables));
    GetPropType<DarcyTypeTag, Properties::IOFields>::initOutputModule(darcyVtkWriter);
    darcyVtkWriter.write(0.0);

    // get some time loop parameters
    using Scalar = GetPropType<DarcyTypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // instantiate time loop
    auto timeLoop = std::make_shared<TimeLoop<Scalar>>(0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<DarcyTypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(darcyProblem, gridGeometry, darcyGridVariables, timeLoop);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // time loop
    timeLoop->start(); do
    {
        // set previous solution for storage evaluations
        assembler->setPreviousSolution(solOld);

        // solve the non-linear system with time step control
        nonLinearSolver.solve(sol, *timeLoop);

        // make the new solution the old solution
        solOld = sol;

        // advance grid variables to the next time step
        darcyGridVariables->advanceTimeStep();

        // update the auxiliary free flow solution vector
        assembler->updateGridVariables(sol);

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // write vtk output
        darcyVtkWriter.write(timeLoop->time());

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

    } while (!timeLoop->finished());

    timeLoop->finalize(darcyGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
