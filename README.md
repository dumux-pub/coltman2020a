This repository contains all of the required files and tests to reproduce the results shown in 
[Obstacles, Interfacial Forms, and Turbulence: A Numerical Analysis of Soil–Water Evaporation Across Different Interfaces](https://doi.org/10.1007/s11242-020-01445-6)

``` bash
@article{Coltman2020a, 
  author = {Coltman, Edward and Lipp, Melanie and Vescovini, Andrea and Helmig, Rainer}, 
  title = {{Obstacles, Interfacial Forms, and Turbulence: A Numerical Analysis of Soil–Water Evaporation Across Different Interfaces}}, 
  issn = {0169-3913}, 
  doi = {10.1007/s11242-020-01445-6}, 
  pages = {1--27}, 
  journal = {Transport in Porous Media}, 
  year = {2020}
}
```

To install this repository, download the [Install Script](https://git.iws.uni-stuttgart.de/dumux-pub/coltman2020a/-/blob/master/installdumux.sh) and run it in a location of your choice. 

``` bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/coltman2020a/-/raw/master/installdumux.sh
sh installdumux.sh
```

This will download all dependent dune and dumux releases and apply any patches needed. 

To build the coupled tests, change the directory to the build directory, and run all tests with the multidomain label:
``` bash
cd ./dumuxColtman2020a/coltman2020a/build-cmake/
make build_multidomain_tests
```
and run the individual tests (an example here from the obstacle height section, and the 10cm obstacle) with: 
``` bash
cd ./appl/height/multidomain/
./test_md_darcy2cni_rans2cni_height params_height100.input
```
or run a series of tests with the `run.sh` script linked in each testing folder. 